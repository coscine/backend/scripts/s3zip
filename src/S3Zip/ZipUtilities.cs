﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace Coscine.S3Zip
{
    public class ZipUtilities
    {
        public S3MultipartStreamConfiguration S3ZipConfiguration { get; set; }

        public ZipUtilities(S3MultipartStreamConfiguration s3ZipConfiguration)
        {
            S3ZipConfiguration = s3ZipConfiguration;
        }

        public void ZipFolder(string path, ZipArchive zipArchive)
        {
            ZipFolder(new DirectoryInfo(path), zipArchive);
        }

        public void ZipFolder(DirectoryInfo directoryInfo, ZipArchive zipArchive)
        {
            var prefixLength = directoryInfo.FullName.EndsWith($"{Path.DirectorySeparatorChar}") ? directoryInfo.FullName.Length : directoryInfo.FullName.Length + 1;
            ZipFiles(directoryInfo.GetFiles("*", SearchOption.AllDirectories), prefixLength, zipArchive);
        }

        public void ZipFiles(IEnumerable<FileInfo> files, int prefixLength, ZipArchive zipArchive)
        {
            foreach (var file in files)
            {
                // cut off parent directory path
                var fileEntry = zipArchive.CreateEntry(file.FullName.Substring(prefixLength));
                using (var fileStream = file.OpenRead())
                {
                    ZipStream(fileStream, fileEntry);
                }
            }
        }

        public void ZipStream(Stream stream, ZipArchiveEntry fileEntry)
        {
            using (var entryStream = fileEntry.Open())
            {
                stream.CopyTo(entryStream);
            }
        }


        public void ZipAndUploadFolder(string path)
        {
            ZipAndUploadFolder(new DirectoryInfo(path));
        }

        public void ZipAndUploadFolder(DirectoryInfo directoryInfo)
        {
            ZipAndUploadFolder(directoryInfo, directoryInfo.Name);
        }

        public void ZipAndUploadFolder(string path, string key)
        {
            ZipAndUploadFolder(new DirectoryInfo(path), key);
        }

        public void ZipAndUploadFolder(DirectoryInfo directoryInfo, string key)
        {
            using (var s3Stream = new S3MultipartUploadStream(key, S3ZipConfiguration))
            {
                using (var archive = new ZipArchive(s3Stream, ZipArchiveMode.Create, true))
                {
                    ZipFolder(directoryInfo, archive);
                }
            }
        }
        public void DownloadAndUnzipToFolder(string key, string path)
        {
            DownloadAndUnzipToFolder(key, new DirectoryInfo(path));
        }

        public void DownloadAndUnzipToFolder(string key, DirectoryInfo directoryInfo)
        {
            using (var s3MultipartDownloadStream = new S3MultipartDownloadStream(key, S3ZipConfiguration))
            {
                using (var archive = new ZipArchive(s3MultipartDownloadStream, ZipArchiveMode.Read))
                {
                    UnzipToFolder(archive, directoryInfo);
                }
            }
        }

        public void UnzipToFolder(ZipArchive archive, string path)
        {
            UnzipToFolder(archive, new DirectoryInfo(path));
        }

        public void UnzipToFolder(ZipArchive archive, DirectoryInfo directoryInfo)
        {
            foreach (ZipArchiveEntry entry in archive.Entries)
            {
                var fileName = Path.Combine(directoryInfo.FullName, entry.FullName);

                if (!Directory.Exists(Path.GetDirectoryName(fileName)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fileName));
                }

                using (var fileStream = new FileStream(fileName, FileMode.CreateNew))
                {
                    using (var entryStream = entry.Open())
                    {
                        entryStream.CopyTo(fileStream);
                    }
                }
            }
        }
    }
}
