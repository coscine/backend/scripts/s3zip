﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace Coscine.S3Zip
{
    public class S3MultipartUploadStream : Stream
    {
        private readonly Stream _internalStream;
        private readonly InitiateMultipartUploadResponse _initiateMultipartUploadResponse;
        private readonly List<UploadPartResponse> _uploadPartResponses;
        private int _part;
        private long _length;
        private long _position;

        public string Key { get; }

        public S3MultipartStreamConfiguration S3MultipartStreamConfiguration { get; set; }

        public override bool CanRead => false;

        public override bool CanSeek => false;

        public override bool CanWrite => true;

        public override long Length => _length + _internalStream.Length;

        public override long Position { get => _position + _internalStream.Position; set => throw new NotImplementedException(); }

        public S3MultipartUploadStream(string key, S3MultipartStreamConfiguration s3MultipartStreamConfiguration, Stream internalStream)
        {
            S3MultipartStreamConfiguration = s3MultipartStreamConfiguration;

            _internalStream = internalStream;
            _uploadPartResponses = new List<UploadPartResponse>();
            _part = 0;
            _length = 0;
            _position = 0;

            Key = key;

            var initiateRequest = new InitiateMultipartUploadRequest
            {
                BucketName = S3MultipartStreamConfiguration.BucketName,
                Key = key
            };

            using (var client = new AmazonS3Client(S3MultipartStreamConfiguration.AccessKey, S3MultipartStreamConfiguration.SecretKey, S3MultipartStreamConfiguration.AmazonS3Config))
            {
                _initiateMultipartUploadResponse = client.InitiateMultipartUpload(initiateRequest);
            }
        }

        public S3MultipartUploadStream(string key, S3MultipartStreamConfiguration s3MultipartStreamConfiguration) : this(key, s3MultipartStreamConfiguration, new MemoryStream(s3MultipartStreamConfiguration.ChunckSize))
        {

        }

        public override void Flush()
        {
            _internalStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            int written = 0;
            while (written < count)
            {
                if (count - written + _internalStream.Length <= S3MultipartStreamConfiguration.ChunckSize)
                {
                    _internalStream.Write(buffer, offset + written, count - written);
                    break;
                }
                else
                {
                    int toBeWritten = S3MultipartStreamConfiguration.ChunckSize - (int)_internalStream.Length;
                    _internalStream.Write(buffer, offset + written, toBeWritten);
                    written += toBeWritten;
                    UploadCurrentPart();
                }
            }
        }

        private void UploadCurrentPart()
        {
            UploadPart(_part);
            _part++;
        }

        private void UploadPart(int part)
        {
            _internalStream.Position = 0;
            using (var client = new AmazonS3Client(S3MultipartStreamConfiguration.AccessKey, S3MultipartStreamConfiguration.SecretKey, S3MultipartStreamConfiguration.AmazonS3Config))
            {
                var uploadPartRequest = new UploadPartRequest
                {
                    BucketName = S3MultipartStreamConfiguration.BucketName,
                    Key = Key,
                    UploadId = _initiateMultipartUploadResponse.UploadId,
                    PartNumber = part + 1,
                    PartSize = _internalStream.Length,
                    InputStream = _internalStream
                };

                _uploadPartResponses.Add(client.UploadPart(uploadPartRequest));
            }

            _position += _internalStream.Position;
            _length += _internalStream.Length;
            _internalStream.SetLength(0);
        }

        private void FinalizeS3MultipartUpload()
        {
            var completeRequest = new CompleteMultipartUploadRequest
            {
                BucketName = S3MultipartStreamConfiguration.BucketName,
                Key = Key,
                UploadId = _initiateMultipartUploadResponse.UploadId
            };

            completeRequest.AddPartETags(_uploadPartResponses);

            using (var client = new AmazonS3Client(S3MultipartStreamConfiguration.AccessKey, S3MultipartStreamConfiguration.SecretKey, S3MultipartStreamConfiguration.AmazonS3Config))
            {
                client.CompleteMultipartUpload(completeRequest);
            }
        }

        // Finalize on stream close clall.
        public override void Close()
        {
            if(_internalStream.Length > 0)
            {
                UploadCurrentPart();
            }

            FinalizeS3MultipartUpload();

            _internalStream.Close();
        }
    }
}
