﻿using Amazon.S3;
using System;

namespace Coscine.S3Zip
{
    public class S3MultipartStreamConfiguration
    {
        public string AccessKey { get; set; }

        public string SecretKey { get; set; }

        public string BucketName { get; set; }

        public AmazonS3Config AmazonS3Config { get; set; } 

        public int ChunckSize { get; set; } = 50 * (int)Math.Pow(2, 20);
    }
}
