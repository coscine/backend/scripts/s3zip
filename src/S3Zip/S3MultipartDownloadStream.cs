﻿using Amazon.S3;
using Amazon.S3.Model;
using System.IO;

namespace Coscine.S3Zip
{
    public class S3MultipartDownloadStream : Stream
    {
        private readonly Stream _internalStream;
        private readonly long _length;
        private long _position;

        public string Key { get; }

        public S3MultipartStreamConfiguration S3MultipartStreamConfiguration { get; set; }

        public override bool CanRead => true;

        public override bool CanSeek => true;

        public override bool CanWrite => false;

        public override long Length => _length;

        public override long Position { get => _position + _internalStream.Position; set => Seek(value - Position, SeekOrigin.Current); }

        public S3MultipartDownloadStream(string key, S3MultipartStreamConfiguration s3MultipartStreamConfiguration, Stream internalStream)
        {
            S3MultipartStreamConfiguration = s3MultipartStreamConfiguration;

            _internalStream = internalStream;
            _position = 0;

            Key = key;

            using (var client = new AmazonS3Client(S3MultipartStreamConfiguration.AccessKey, S3MultipartStreamConfiguration.SecretKey, S3MultipartStreamConfiguration.AmazonS3Config))
            {
                var meta = client.GetObjectMetadata(new GetObjectMetadataRequest()
                {
                    BucketName = S3MultipartStreamConfiguration.BucketName,
                    Key = key
                });

                _length = meta.Headers.ContentLength;
            }

            DownloadRange(new ByteRange(0, Position + S3MultipartStreamConfiguration.ChunckSize - 1));
        }

        public S3MultipartDownloadStream(string key, S3MultipartStreamConfiguration s3MultipartStreamConfiguration) : this(key, s3MultipartStreamConfiguration, new MemoryStream(s3MultipartStreamConfiguration.ChunckSize))
        {

        }

        private void DownloadRange(ByteRange range)
        {
            using (var client = new AmazonS3Client(S3MultipartStreamConfiguration.AccessKey, S3MultipartStreamConfiguration.SecretKey, S3MultipartStreamConfiguration.AmazonS3Config))
            {
                var request = new GetObjectRequest
                {
                    BucketName = S3MultipartStreamConfiguration.BucketName,
                    Key = Key,
                    ByteRange = range
                };

                _internalStream.SetLength(0);

                using (var response = client.GetObject(request))
                {
                    response.ResponseStream.CopyTo(_internalStream);
                }
            }

            _internalStream.Position = 0;
        }

        public override void Flush()
        {
            _internalStream.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int read = 0;

            while (read < count && Position < Length)
            {
                if (_internalStream.Length -_internalStream.Position >= count - read)
                {
                    read += _internalStream.Read(buffer, offset + read, count - read);
                    break;
                }
                else
                {
                    int toBeRead = (int)(_internalStream.Length - _internalStream.Position);
                    read += _internalStream.Read(buffer, offset + read, toBeRead);
                    var oldPosition = _internalStream.Position;
                    DownloadRange(new ByteRange(Position, Position + S3MultipartStreamConfiguration.ChunckSize - 1));
                    _position += oldPosition;
                }
            }

            return read;
        }


        private ByteRange CalculateByteRange(long position)
        { 
            long leftPosition;
            long rightPosition;

            if (position - (S3MultipartStreamConfiguration.ChunckSize / 2) < 0
            && position + (S3MultipartStreamConfiguration.ChunckSize / 2) + S3MultipartStreamConfiguration.ChunckSize % 2 - 1 >= Length)
            {
                // both to small
                leftPosition = 0;
                rightPosition = Length - 1;
            }
            else if (position - (S3MultipartStreamConfiguration.ChunckSize / 2) < 0)
            {
                // left to small
                leftPosition = 0;
                rightPosition = S3MultipartStreamConfiguration.ChunckSize - 1;
            }
            else if (position + (S3MultipartStreamConfiguration.ChunckSize / 2) + S3MultipartStreamConfiguration.ChunckSize % 2 - 1 >= Length)
            {
                // right to small
                leftPosition = Length - S3MultipartStreamConfiguration.ChunckSize - 1;
                rightPosition = Length - 1;
            }
            else
            {
                // fits perfectly
                leftPosition = position - (S3MultipartStreamConfiguration.ChunckSize / 2);
                rightPosition = position + (S3MultipartStreamConfiguration.ChunckSize / 2) + S3MultipartStreamConfiguration.ChunckSize % 2 - 1;
            }

            return new ByteRange(leftPosition, rightPosition);
        }


        public override long Seek(long offset, SeekOrigin origin)
        {
            //var a = _internalStream.Seek(offset, origin);
            //return a;
            
            var position = 0L;

            switch (origin)
            {
                case SeekOrigin.Begin:
                    position = offset;
                    break;
                case SeekOrigin.Current:
                    position = Position + offset;
                    break;
                case SeekOrigin.End:
                    position = Length + offset;
                    break;
            }

            var realOffset = position - Position;

            // is the position still buffered?
            if ((realOffset >= 0 && _internalStream.Position + realOffset < _internalStream.Length) 
                || (realOffset < 0 && _internalStream.Position + realOffset >= 0))
            {
                _internalStream.Position += realOffset;
            } else
            {
                var range = CalculateByteRange(position);
                DownloadRange(range);
                _internalStream.Position = position - range.Start;
                _position = range.Start;
            }

            return Position;
        }

        public override void SetLength(long value)
        {
            throw new System.NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new System.NotImplementedException();
        }
    }
}
