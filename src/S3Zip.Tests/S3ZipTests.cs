﻿
using Amazon.S3;
using Amazon.S3.Model;
using NUnit.Framework;
using System;
using System.IO.Compression;
using System.Linq;

namespace Coscine.S3Zip.Tests
{
    [TestFixture]
    public class S3ZipTests
    {
        private readonly S3MultipartStreamConfiguration _s3MultipartStreamConfiguration;
        private readonly char[] _charArray;
        private readonly int _ONE_MB;

        public S3ZipTests()
        {
            var accessKey = Environment.GetEnvironmentVariable("S3_ACCESS_KEY", EnvironmentVariableTarget.Process);
            var secretKey = Environment.GetEnvironmentVariable("S3_SECRET_KEY", EnvironmentVariableTarget.Process);

            _charArray = "$%#@!*abcdefghijklmnopqrstuvwxyz1234567890?;:ABCDEFGHIJKLMNOPQRSTUVWXYZ^&".ToCharArray();

            _ONE_MB = (int)Math.Pow(2, 20);

            _s3MultipartStreamConfiguration = new S3MultipartStreamConfiguration
            {
                AccessKey = accessKey,
                SecretKey = secretKey,
                BucketName = "PITLABTTEST",
                AmazonS3Config = new AmazonS3Config
                {
                    ServiceURL = "https://s3.rwth-aachen.de/",
                    ForcePathStyle = true
                }
            };
        }

        [Test]
        [Ignore("Meant for manual testing.")]
        public void TestUpload()
        {
            var key = "test.zip";
            // uploading
            var zipUtilities = new ZipUtilities(_s3MultipartStreamConfiguration);
            zipUtilities.ZipAndUploadFolder(@"c:\temp\test", key);
        }

        [Test]
        [Ignore("Meant for manual testing.")]
        public void TestDownload()
        {
            var key = "test.zip";

            var zipUtilities = new ZipUtilities(_s3MultipartStreamConfiguration);
            zipUtilities.DownloadAndUnzipToFolder(key, @"c:\temp\test2\");
        }

        [TestCase(110, 50)]
        [TestCase(110, 5)]
        [TestCase(30, 50)]
        [TestCase(30, 5)]
        public void S3MultipartUploadStreamTest(long length, int chunkSize)
        {
            var key = "S3MultipartUploadStreamTest.txt";
            var seed = DateTime.Now.Millisecond;
            var random = new Random(seed);
            var randomBuffer = new byte[_ONE_MB];

            var oldChunkSize = _s3MultipartStreamConfiguration.ChunckSize;
            _s3MultipartStreamConfiguration.ChunckSize = chunkSize * _ONE_MB;

            using (var s3Stream = new S3MultipartUploadStream(key, _s3MultipartStreamConfiguration))
            {
                for (int i = 0; i < length; i++)
                {
                    FillArrayWithRandomChars(randomBuffer, randomBuffer.Length, _charArray, random);
                    s3Stream.Write(randomBuffer, 0, _ONE_MB);
                }
            }

            var request = new GetObjectRequest
            {
                BucketName = _s3MultipartStreamConfiguration.BucketName,
                Key = key
            };

            // reset the random number generator
            random = new Random(seed);
            var buffer = new byte[_ONE_MB];

            using (var client = new AmazonS3Client(_s3MultipartStreamConfiguration.AccessKey, _s3MultipartStreamConfiguration.SecretKey, _s3MultipartStreamConfiguration.AmazonS3Config))
            {
                var meta = client.GetObjectMetadata(new GetObjectMetadataRequest()
                {
                    BucketName = _s3MultipartStreamConfiguration.BucketName,
                    Key = key
                });

                var fileSize = meta.Headers.ContentLength;
                Assert.IsTrue(fileSize == length * buffer.Length);

                using (var response = client.GetObject(request))
                {
                    // Was request successfull?
                    Assert.IsTrue(response.HttpStatusCode == System.Net.HttpStatusCode.OK);
                    // Has the correct length?
                    Assert.IsTrue(response.ContentLength == length * _ONE_MB);

                    using (var responseStream = response.ResponseStream)
                    {
                        long alreadyRead = 0;
                        while (alreadyRead < length * buffer.Length)
                        {
                            int read = responseStream.Read(buffer, 0, buffer.Length);
                            FillArrayWithRandomChars(randomBuffer, read, _charArray, random);
                            // check if the data is equal to the previously generated one.
                            Assert.IsTrue(Enumerable.SequenceEqual(randomBuffer.Take(read), buffer.Take(read)));
                            alreadyRead += read;
                        }
                        Assert.IsTrue(alreadyRead == fileSize);
                    }
                }
            }
            _s3MultipartStreamConfiguration.ChunckSize = oldChunkSize;
        }

        [TestCase(110, 50)]
        [TestCase(110, 5)]
        [TestCase(30, 50)]
        [TestCase(30, 5)]
        public void S3MultipartDownloadStreamTest(long length, int chunkSize)
        {
            var key = "S3MultipartDownloadStreamTest.txt";
            var seed = DateTime.Now.Millisecond;
            var random = new Random(seed);
            var randomBuffer = new byte[_ONE_MB];

            var oldChunkSize = _s3MultipartStreamConfiguration.ChunckSize;
            _s3MultipartStreamConfiguration.ChunckSize = chunkSize * _ONE_MB;

            using (var s3Stream = new S3MultipartUploadStream(key, _s3MultipartStreamConfiguration))
            {
                for (int i = 0; i < length; i++)
                {
                    FillArrayWithRandomChars(randomBuffer, randomBuffer.Length, _charArray, random);
                    s3Stream.Write(randomBuffer, 0, _ONE_MB);
                }
            }

            // reset the random number generator
            random = new Random(seed);
            var buffer = new byte[_ONE_MB];

            using (var client = new AmazonS3Client(_s3MultipartStreamConfiguration.AccessKey, _s3MultipartStreamConfiguration.SecretKey, _s3MultipartStreamConfiguration.AmazonS3Config))
            {
                var meta = client.GetObjectMetadata(new GetObjectMetadataRequest()
                {
                    BucketName = _s3MultipartStreamConfiguration.BucketName,
                    Key = key
                });

                var fileSize = meta.Headers.ContentLength;
                Assert.IsTrue(fileSize == length * buffer.Length);

                using (var s3MultipartDownloadStream = new S3MultipartDownloadStream(key, _s3MultipartStreamConfiguration))
                {
                    long alreadyRead = 0;
                    while (alreadyRead < length * buffer.Length)
                    {
                        int read = s3MultipartDownloadStream.Read(buffer, 0, buffer.Length);
                        FillArrayWithRandomChars(randomBuffer, read, _charArray, random);
                        // check if the data is equal to the previously generated one.
                        Assert.IsTrue(Enumerable.SequenceEqual(randomBuffer.Take(read), buffer.Take(read)));
                        alreadyRead += read;
                    }
                    Assert.IsTrue(alreadyRead == fileSize);
                }
            }
            _s3MultipartStreamConfiguration.ChunckSize = oldChunkSize;
        }

        private void FillArrayWithRandomChars(byte[] randomBuffer, int length, char[] chars, Random random)
        {
            for (int i = 0; i < length; i++)
            {
                randomBuffer[i] = (byte)chars[random.Next(chars.Length)];
            }
        }
    }
}
