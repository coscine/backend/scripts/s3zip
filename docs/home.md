## Library for uploading files to S3.

The allows to zip a folder and/or upload a whole stream in chunks to a S3 object store.

For example:
```
// uploading
var zipUtilities = new ZipUtilities(s3MultipartStreamConfiguration);
zipUtilities.ZipAndUploadFolder(@"c:\temp\test", "test.zip");
```

```
// downloading
var zipUtilities = new ZipUtilities(_s3MultipartStreamConfiguration);
using (var s3MultipartDownloadStream = new S3MultipartDownloadStream(key, s3MultipartStreamConfiguration))
{
	using (var archive = new ZipArchive(s3MultipartDownloadStream, ZipArchiveMode.Read, true))
	{
		zipUtilities.UnzipToFolder(archive, @"c:\temp\test2\");
	}
}
```

A zip file of the folder will be in S3 under the key test.zip.